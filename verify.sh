#!/bin/bash
set -Eeuo pipefail

cd "$(dirname "$BASH_SOURCE")"

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
	versions=( */*/ )
fi
versions=( "${versions[@]%/}" )

badness=

gpgFingerprint="$(grep -v '^#' gpg-fingerprint 2>/dev/null || true)"
if [ -z "$gpgFingerprint" ]; then
	echo >&2 'warning: missing gpg-fingerprint! skipping PGP verification!'
	badness=1
else
	export GNUPGHOME="$(mktemp -d)"
	trap "gpgconf --kill all || :; rm -rf '$GNUPGHOME'" EXIT
	gpg --keyserver keyserver.ubuntu.com --recv-keys "$gpgFingerprint"
fi

hostArch="$(dpkg --print-architecture)"

for v in "${versions[@]}"; do
	arch=${v#*/}
	codename=${v%/*}

	baseOciUrl="https://partner-images.canonical.com/oci/$codename/current"
	if ! wget -q --spider "$baseOciUrl/ubuntu-$codename-oci-$arch.manifest"; then
		thisTarBase="ubuntu-$codename-core-cloudimg-$arch"
		thisTar="$thisTarBase-root.tar.gz"
		sumTypes=( sha256 sha1 md5 )
	else
		thisTarBase="ubuntu-$codename-oci-$arch"
		thisTar="$thisTarBase-root.tar.gz"
		sumTypes=( sha256 )
	fi
	for sums in "${sumTypes[@]}"; do
		sumsFile="$codename/${sums^^}SUMS" # "SHA256SUMS"
		sumCmd="${sums}sum" # "sha256sum"
		if [ -n "$gpgFingerprint" ]; then
			if [ ! -f "$sumsFile.gpg" ]; then
				echo >&2 "warning: '$sumsFile.gpg' appears to be missing!"
				badness=1
			else
				( set -x; gpg --batch --verify "$sumsFile.gpg" "$sumsFile" )
			fi
		fi
		if [ -s "$sumsFile" ]; then
			grep " *$thisTar\$" "$sumsFile" | ( set -x; cd "$v" && "$sumCmd" -c - )
		else
			echo >&2 "warning: missing '$sumsFile'!"
			badness=1
		fi
	done
done

if [ "$badness" ]; then
	false
fi
