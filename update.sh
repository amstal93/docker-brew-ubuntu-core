#!/bin/bash
set -Eeuo pipefail

cd "$(dirname "$BASH_SOURCE")"

needUpdateCiYml=

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
	versions=( */*/ )
	needUpdateCiYml=yes
fi
versions=( "${versions[@]%/}" )

hostArch="$(dpkg --print-architecture)"

qemuNativeArches=(amd64-i386 arm-armel armel-arm arm-armhf armhf-arm armel-armhf armhf-armel i386-amd64 powerpc-ppc64 ppc64-powerpc sparc-sparc64 sparc64-sparc s390-s390x s390x-s390)
for arch in alpha arm armeb i386 m68k mips mipsel mips64el ppc64 riscv32 riscv64 sh4 sh4eb sparc sparc64 s390x; do
	eval "qemuArch_$arch=$arch"
done
qemuArch_amd64=x86_64
qemuArch_armel=arm
qemuArch_armhf=arm
qemuArch_arm64=aarch64
qemuArch_lpia=i386
qemuArch_powerpc=ppc
qemuArch_powerpcspe=ppc
qemuArch_ppc64el=ppc64le
qemuSuite_trusty=bionic
qemuSuite_artful=bionic
qemuSuite_xenial=bionic

isEol() {
	local var_name="isEol_$1"
	local eol=${!var_name:-}

	if [ -z "${eol}" ]; then
		if wget -q --spider "http://archive.ubuntu.com/ubuntu/dists/$1/Release"; then
			eol="no"
		else
			eol="yes"
		fi
		eval "${var_name}=${eol}"
	fi
	[ "${eol}" = "yes" ] || return 1
}

gitlabCiJobs=
toVerify=()
for v in "${versions[@]}"; do
	arch=${v#*/}
	codename=${v%/*}

	if ! grep -qE "^$arch\$" "$codename/arches"; then
		continue
	fi

	gitlabCiJobs+="build:${codename}:${arch}:"

	baseOciUrl="https://partner-images.canonical.com/oci/$codename/current"
	if ! wget -q --spider "$baseOciUrl/ubuntu-$codename-oci-$arch.manifest"; then
		thisTarBase="ubuntu-$codename-core-cloudimg-$arch"
		thisTar="$thisTarBase-root.tar.gz"
		baseUrl="https://partner-images.canonical.com/core/$codename/current"
		if ! wget -q --spider "$baseUrl/$thisTarBase.manifest"; then
			baseUrl="https://partner-images.canonical.com/core/unsupported/$codename/current"
		fi
		wget -c -qN -P "$codename" "$baseUrl/"{MD5,SHA{1,256}}SUMS{,.gpg}
	else
		thisTarBase="ubuntu-$codename-oci-$arch"
		thisTar="$thisTarBase-root.tar.gz"
		baseUrl="$baseOciUrl"
		wget -c -qN -P "$codename" "$baseUrl/"SHA256SUMS{,.gpg}
	fi

	wget -c -qN -P "$v" "$baseUrl/"{'unpacked/build-info.txt',"$thisTarBase.manifest"}
	wget -c -N -P "$v" --progress=dot:giga "$baseUrl/$thisTar"

	echo -n > "$v/Dockerfile"

	qemuArch=
	qemuSuite=
	if [[ ! " $hostArch-$hostArch ${qemuNativeArches[*]} " =~ $hostArch-$arch ]]; then
		qemuArchVar="qemuArch_$arch"
		qemuArch="${!qemuArchVar}"
		qemuSuiteVar="qemuSuite_$codename"
		qemuSuite="${!qemuSuiteVar:-$codename}"

		cat >> "$v/Dockerfile" <<EOF
FROM ubuntu:$qemuSuite AS qemu
EOF

		if isEol "${qemuSuite}"; then
			cat >> "$v/Dockerfile" <<'EOF'

RUN sed -e 's!archive.ubuntu.com!old-releases.ubuntu.com!' \
	-e 's!security.ubuntu.com!old-releases.ubuntu.com!' \
	-i /etc/apt/sources.list
EOF
		fi

		cat >> "$v/Dockerfile" <<'EOF'

RUN apt-get update --quiet \
	&& apt-get install --no-install-recommends --yes \
		qemu-user-static

EOF

		gitlabCiJobs+="\n  extends: .build-foreign"
		gitlabCiJobs+="\n  needs:"
		gitlabCiJobs+="\n    - build:${codename}:amd64"
	else
		gitlabCiJobs+="\n  extends: .build-native"
	fi

	cat >> "$v/Dockerfile" <<EOF
FROM scratch
ADD $thisTar /
EOF

	if [ -n "$qemuArch" ]; then
		cat >> "$v/Dockerfile" <<EOF
COPY --from=qemu /usr/bin/qemu-${qemuArch}-static /usr/bin
EOF
	fi

	if isEol "${codename}"; then
		cat >> "$v/Dockerfile" <<'EOF'

RUN sed -e 's!archive.ubuntu.com!old-releases.ubuntu.com!' \
	-e 's!security.ubuntu.com!old-releases.ubuntu.com!' \
	-e 's!ports.ubuntu.com/ubuntu-ports!old-releases.ubuntu.com/ubuntu!' \
	-i /etc/apt/sources.list
EOF
	fi

	if [ "$baseUrl" != "$baseOciUrl" ]; then
		cat >> "$v/Dockerfile" <<'EOF'

# a few minor docker-specific tweaks
# see https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap
RUN set -xe \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L40-L48
	&& echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
	&& echo 'exit 101' >> /usr/sbin/policy-rc.d \
	&& chmod +x /usr/sbin/policy-rc.d \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L54-L56
	&& dpkg-divert --local --rename --add /sbin/initctl \
	&& cp -a /usr/sbin/policy-rc.d /sbin/initctl \
	&& sed -i 's/^exit.*/exit 0/' /sbin/initctl \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L71-L78
	&& echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L85-L105
	&& echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean \
	&& echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean \
	&& echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L109-L115
	&& echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L118-L130
	&& echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes \
	\
# https://github.com/docker/docker/blob/9a9fc01af8fb5d98b8eec0740716226fadb3735c/contrib/mkimage/debootstrap#L134-L151
	&& echo 'Apt::AutoRemove::SuggestsImportant "false";' > /etc/apt/apt.conf.d/docker-autoremove-suggests
EOF

		case "$codename" in
		'precise'|'utopic'|'vivid'|'wily'|'xenial'|'artful')
			cat >> "$v/Dockerfile" <<'EOF'

# delete all the apt list files since they're big and get stale quickly
RUN rm -rf /var/lib/apt/lists/*
# this forces "apt-get update" in dependent images, which is also good
# (see also https://bugs.launchpad.net/cloud-images/+bug/1699913)
EOF
			;;
		*)
			cat >> "$v/Dockerfile" <<'EOF'

# verify that the APT lists files do not exist
RUN [ -z "$(apt-get indextargets)" ]
# (see https://bugs.launchpad.net/cloud-images/+bug/1699913)
EOF
			;;
		esac

		if ! (tar xf "$v/$thisTar" -O etc/apt/sources.list | grep -E '^deb .* universe'); then
			cat >> "$v/Dockerfile" <<'EOF'

# enable the universe
RUN sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list
EOF
		fi

		cat >> "$v/Dockerfile" <<'EOF'

# make systemd-detect-virt return "docker"
# See: https://github.com/systemd/systemd/blob/aa0c34279ee40bce2f9681b496922dedbadfca19/src/basic/virt.c#L434
RUN mkdir -p /run/systemd && echo 'docker' > /run/systemd/container

EOF
	fi

	cat >> "$v/Dockerfile" <<-EOF
		CMD ["/bin/bash"]
	EOF

	toVerify+=( "$v" )

	if isEol "${codename}"; then
		gitlabCiJobs+="\n  rules:"
		gitlabCiJobs+="\n    - if: (\$CI_COMMIT_REF_NAME == \$CI_DEFAULT_BRANCH || \$CI_COMMIT_TAG) && \$INCLUDE_EOL"
		gitlabCiJobs+="\n    - if: \$CI_MERGE_REQUEST_IID"
	fi

	gitlabCiJobs+="\n\n"
done

if [ "${#toVerify[@]}" -gt 0 ]; then
	( set -x; ./verify.sh "${toVerify[@]}" )
fi

if [ -n "${needUpdateCiYml}" ]; then
  gitlabCi="$(awk -v 'RS=\n\n' '/^## BEGIN build JOBS ##/,/^## END build JOBS ##/ { if ($0 ~ /^## BEGIN/) $0 = "## BEGIN build JOBS ##\n\n'"${gitlabCiJobs}"'## END build JOBS ##\n\n"; else next; } { printf "%s%s", $0, RS }' .gitlab-ci.yml)"
  echo "$gitlabCi" > .gitlab-ci.yml
fi
